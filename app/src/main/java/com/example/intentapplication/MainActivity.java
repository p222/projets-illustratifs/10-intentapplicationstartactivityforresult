package com.example.intentapplication;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    static int CODE_REQUEST_IMAGE = 54321;
    static String TAG = "MainActivity:LOG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layoutactivite1);


        // Mise en place du traitement sur le bouton ...
        Button bouton = (Button) findViewById(R.id.idButton);

        bouton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAppareilPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                try {
                    startActivityForResult(intentAppareilPhoto, CODE_REQUEST_IMAGE);
                } catch (ActivityNotFoundException err) {
                    Log.e(MainActivity.TAG, "Application appareil photo inconnue");
                }
            }
        });
    }


    // Traitement de la réponse de l'activité appelée
    // resultCode indique si l'activité s'est bien déroulée
    // requestCode correspond au code de l'activité appelée pour la reconnaitre
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CODE_REQUEST_IMAGE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            // Récupère l'image retournée par l'activité sous forme d'une image bitmap
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            ImageView imageView = (ImageView)findViewById(R.id.idImageView);
            imageView.setImageBitmap(imageBitmap);
        }
    }
}